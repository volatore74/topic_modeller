[On line version](https://gitlab.com/volatore74/topic_modeller/-/blob/master/README.md)

## Is there any refactoring you’d suggest to make the code cleaner, less CPU intensive, less buggy, follow some guidelines?\*\*

### generally

-  document the code (none of the function is documented)
-  no type hints
-  no docstrings
-  wrap the model code to applies same logic used by `run_bulk` and `run_interactively`
-  use context handler for the file handling

### looking at the code

-  **use os.path.join or pathlib.Path in places like here and here to avoid weird paths/bugs**

   ```
   8:  CONFIG = CWD + '/config.json'
   55: classes_dir = CWD.replace('/app','') + '/tf_model/classes.dict'
   ```

-  **use the **context handler** while handling file access**

   ```
   56: mappings = [line.rstrip('\n') for line in open(classes_dir)]
   ```

-  **Wrap the model code in a class that will also handle additional logic related to the model**

   ```
   16: predictions = MODEL([message])

   29: if greater_than_length_threshold(message):
           predictions = MODEL([message])
           topic_thresholds = map_labels_to_threshold(predictions, mappings)
           print(topic_thresholds)
   ```

-  **Avoid unpythonic code, follow the style guide (PEP8)**

   ```
        message = row['x']
        truth   = row['y'] <-----
   ```

   This is a minor thing and it only concern the readability, but makes me aware propably no linter have been used. \
   Tools like Flake8 or any other linter will save use to have things like `unused import or variable` that makes the code havier to deploy and run
   <br/>

-  **run_bulk** and **run_interactively** \
   don't use the same logic (ie **map_labels_to_threshold**)\
   Wrap the model code in a class that has a method for predict that applies same logic

## What aspects are we missing?

-  **Containerisation** (docker/kube)
   A virtualenv **only encapsulates Python dependencies**. A Docker container **encapsulates an entire OS**.
   With Docker, you can swap out the entire OS installing and running Python (or any other language) on Ubuntu, Debian, Alpine, even Windows Server Core.
-  **A way to enqueue messages to be inferred by the model.**
   If you are serving this model online, you may get requests from users for predictions asynchronously.
   Because the model has a set time that it takes to compute (i.e time to run the prediction function), it may be that 100 users try to access the model at the same time. We can build a queue that the model consumes prediction by prediction to ensure all users queries are answered
-  **Is it possible to use multithreading to run this model ?**
   I know they usually are but it may depend on the architecture of the model and the framework used to represent it, e.g tensorflow, pytorch, etc

## What unit tests would you suggest? Should we refactor this to create better test coverage?

-  Test we can load a model
-  Test if we feed a known message in, that we get the right prediction
-  Test we can enqueue a batch of predictions and check if we got the result for all the requests
-  Test the class mapping handling
-  Test the thresholding

## Are there things we need to consider to deploy such a big model (compared to a simple SkLearn example)?

-  Need to store the parameters (model weights) somewhere
-  Need to be able to load such a large model into memory (RAM) or GPU memory (VRAM) depending on use or at least use a cache system (ie loading class mappings)

## What happens if we scale from 10 concurrent users to 100, each of which needs to share the same model asset?

-  Create a queue of input messages from users to get predictions one by one from model that can then be served back to users
-  Reduce the loading time using a cache system beeing able to be used from multiple thread/instances

## What considerations should we be aware of if we want to deploy using GPUs to run our models (cost/usefulness)?

-  GPUs are only really useful for training the model as they allow us to do mini-batch optimisation
-  At inference time we only need to run inference on a single data point, so in most cases GPU not needed.
-  We can use a queue to build batches of input data if we want to use GPUs
-  GPUs increase the cost of hosting models online a lot
